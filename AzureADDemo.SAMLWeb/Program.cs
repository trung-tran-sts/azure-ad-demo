using Microsoft.AspNetCore.Authentication.Cookies;
using Sustainsys.Saml2;
using Sustainsys.Saml2.AspNetCore2;
using Sustainsys.Saml2.Metadata;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var services = builder.Services;

services.AddAuthentication(options =>
{
    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = Saml2Defaults.Scheme;
})
.AddSaml2(Saml2Defaults.Scheme, options =>
{
    options.SPOptions.EntityId = new EntityId("https://localhost:7032");
    options.IdentityProviders.Add(new IdentityProvider(
        new EntityId("https://sts.windows.net/abcdf23e-ded0-4cc4-add9-24bb48b93b3c/"), options.SPOptions)
    {
        MetadataLocation = "https://login.microsoftonline.com/abcdf23e-ded0-4cc4-add9-24bb48b93b3c/federationmetadata/2007-06/federationmetadata.xml?appid=23c8705d-05fe-4121-aaa4-3b943a9bd94a"
    });
})
.AddCookie(CookieAuthenticationDefaults.AuthenticationScheme);

services.AddAuthorization();

services.AddRazorPages(opt =>
{
    opt.Conventions.AuthorizeFolder("/");
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.MapRazorPages();

app.Run();
